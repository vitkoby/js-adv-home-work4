// Ответы на вопросы: 
// AJAX позволяет делать динамические запросы без перезагрузки страницы.

function getFilms() {
	fetch("https://ajax.test-danit.com/api/swapi/films")
		.then((response) => response.json())
		.then((films) => displayFilms(films));
}

function displayFilms(films) {
	const filmsList = document.createElement("ul");
	films.forEach(function (film) {
		const filmItem = document.createElement("li");
		filmItem.textContent = `Episode ${film.episodeId}: ${film.name} (${film.openingCrawl})`;
		filmsList.appendChild(filmItem);
		filmItem.addEventListener("click", function () {
			getCharacters(film.characters, film.name);
		});
	});
	document.body.appendChild(filmsList);
}

function getCharacters(charactersUrls, filmName) {
	const characters = [];
	charactersUrls.forEach(function (url) {
		fetch(url)
			.then((response) => response.json())
			.then((character) => {
				characters.push(character.name);
				displayCharacters(characters, filmName);
			});
	});
}

function displayCharacters(characters, filmName) {
	const charactersList = document.createElement("ul");
	characters.forEach(function (character) {
		const characterItem = document.createElement("li");
		characterItem.textContent = character;
		charactersList.appendChild(characterItem);
	});
	const filmTitle = document.createElement("h2");
	filmTitle.textContent = filmName;
	document.body.appendChild(filmTitle);
	document.body.appendChild(charactersList);
}
getFilms();









